<!DOCTYPE html>
<header>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../assets/css/estilos.css">
    <link href = "../assets/js/validar.js">
    <title> CRE - GRUPO 5</title>
    
        <ul id="menu">
                <li> <a href="../index.php"> Inicio</a></li>
                <li> <a href="../php/EquipoListado.php"> Equipos</a>
                    <ul>
                        <li> <a href="../php/EquipoAlta.php"> Alta</a></li>
                        <li> <a href="../php/EquipoBorrar.php"> Borrar</a></li>
                        <li> <a href="../php/EquipoModificar.php"> Modificar</a></li>
                    </ul>
                    </li>  <!-- con esta li cierro Equipos -->
                
                <li> <a href="../php/ClienteListado.php"> Clientes</a>
                    <ul>
                        <li> <a href="../php/ClienteAlta.php"> Alta</a></li>
                        <li> <a href="../php/ClienteBorrar.php"> Borrar</a></li>
                        <li> <a href="../php/ClienteModificar.php"> Modificar</a></li>
                    </ul>
                    </li>  <!-- con esta li cierro Clientes -->
                
                <li> <a href="../php/RecepcionListado.php"> Recepciones</a>
                    <ul>
                        <li> <a href="../php/RecepcionAlta.php"> Alta</a></li>
                        <li> <a href="../php/RecepcionBorrar.php"> Borrar</a></li>
                        <li> <a href="../php/RecepcionModificar.php"> Modificar</a></li>
                    </ul>
                    </li>  <!-- con esta li cierro Recepciones -->    
           
           
        </ul>  <!-- cierro id = "menu"-->
        
</header>

<body>
    <br>
        <table border = "1">
            <tr>
                <td>id</td>
                <td>Nombre</td>
                <td>Direccion</td>
                <td>Celular</td>
            </tr>

            <?php 

            include "conexion.php";

            $sql = ("SELECT * FROM clientes"); 
            $resultado = mysqli_query($conexion, $sql);

            while ($mostrar = mysqli_fetch_array ($resultado)){
                ?>
            <tr>
                <td><?php echo $mostrar['id_cliente']?></td>
                <td><?php echo $mostrar['nombre']?></td>
                <td><?php echo $mostrar['direccion']?></td>
                <td><?php echo $mostrar['celular']?></td>
            </tr>
         <?php
            }  //while
            ?>
        </table>
</body>